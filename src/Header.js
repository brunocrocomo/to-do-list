import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';

class Header extends Component {
    render() {
        return (
            <Typography variant='h2' align='center' gutterBottom>
                To-Do List
            </Typography>
        );
    }
}

export default Header;
