import React, { Component } from 'react';

import { observer } from 'mobx-react';

import StoreContext from './StoreContext';

import { ListItem as MuiListItem, Checkbox, ListItemText, IconButton } from '@material-ui/core';
import Delete from '@material-ui/icons/Delete';

const ListItem = observer(
    class ListItem extends Component {
        render() {
            const toDoStore = this.context;

            const checkbox =  
                <Checkbox checked={this.props.checked} onChange={(event, checked) => toDoStore.checkListItem(this.props.id, checked)} />

            const listItemText = this.props.checked ?
                <ListItemText><strike>{this.props.description}</strike></ListItemText> :
                <ListItemText>{this.props.description}</ListItemText>;

            return (
                <MuiListItem>
                    {checkbox}
                    {listItemText}
                    <IconButton color='primary' onClick={() => toDoStore.removeListItem(this.props.id)}>
                        <Delete />
                    </IconButton>
                </MuiListItem>
            );
        }
    }
);

ListItem.contextType = StoreContext;

export default ListItem;
