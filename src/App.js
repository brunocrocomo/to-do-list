import React, { Component } from 'react';

import ToDoStore from './Store';
import StoreContext from './StoreContext';
import ToDoList from './ToDoList';

import DevTools from 'mobx-react-devtools';

const toDoStore = new ToDoStore();

class App extends Component {
    render() {
        return (
            <StoreContext.Provider value={toDoStore}>
                <ToDoList />
                <DevTools />
            </StoreContext.Provider>
        );
    }
}

export default App;
