import React from 'react';

import ToDoStore from './Store';

const StoreContext = React.createContext(new ToDoStore());

export default StoreContext;