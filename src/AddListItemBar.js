import React, { Component } from 'react';

import { decorate, observable } from 'mobx';
import { observer } from 'mobx-react';

import StoreContext from './StoreContext';

import { TextField, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    div: {
        display: 'flex',
        justifyContent: 'space-evenly',
        padding: 10
    }
}

const AddListItemBar = observer(

    class AddListItemBar extends Component {
        constructor(props) {
            super(props);

            this.text = '';

            this.handleTextChange = this.handleTextChange.bind(this);
            this.handleListItemInclusion = this.handleListItemInclusion.bind(this);
        }

        componentDidMount() {
            this.toDoStore = this.context;
        }

        handleTextChange(event) {
            this.text = event.target.value;
        }

        handleListItemInclusion() {
            if (this.text) {
                this.toDoStore.addListItem({ "checked": false, "description": this.text });
                this.text = '';
            }
        }

        render() {
            const { classes } = this.props;

            return (
                <div className={classes.div}>
                    <TextField
                        label='What do you have to do?'
                        value={this.text}
                        onChange={this.handleTextChange} />

                    <Button
                        color='primary'
                        variant='contained'
                        onClick={this.handleListItemInclusion}>
                        Add
                    </Button>
                </div>
            );
        }
    }
);

AddListItemBar.contextType = StoreContext;

decorate(AddListItemBar, {
    text: observable
});

export default withStyles(styles)(AddListItemBar);
