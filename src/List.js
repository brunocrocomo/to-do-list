import React, { Component } from 'react';

import { observer } from 'mobx-react';

import StoreContext from './StoreContext';

import ListItem from './ListItem';

import { List as MuiList, Typography } from '@material-ui/core';

const List = observer(
    class List extends Component {
        render() {
            const toDoStore = this.context;

            const listItems =
                toDoStore.toDoList.map((listItem, index) => {
                    return (
                        <ListItem
                            key={index}
                            id={index}
                            checked={listItem.checked}
                            description={listItem.description}
                        />
                    )
                })

            const emptyListWarning =
                <Typography
                    variant='subtitle1'
                    align='center'
                    gutterBottom>
                    Your To-Do list is empty!
                </Typography>

            return (
                <MuiList>
                    {toDoStore.toDoList.length > 0 ? listItems : emptyListWarning}
                </MuiList >
            );
        }
    }
);

List.contextType = StoreContext;

export default List;
