import React, { Component } from 'react';

import { observer } from 'mobx-react';

import Header from './Header';
import List from './List';
import AddListItemBar from './AddListItemBar';

import Paper from '@material-ui/core/Paper';
import { withStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';


const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#00a19a'
        },
        secondary: {
            main: '#575757'
        }
    },
    typography: {
        useNextVariants: true,
    }
})

const styles = {
    paper: {
        margin: 20,
        padding: 20,
        width: 400
    },
    div: {
        display: 'flex',
        justifyContent: 'center'
    }
}

const ToDoList = observer(
    class ToDoList extends Component {
        render() {
            const { classes } = this.props;

            return (
                <MuiThemeProvider theme={theme}>
                    <div className={classes.div}>
                        <Paper className={classes.paper}>
                            <Header />
                            <List />
                            <AddListItemBar />
                        </Paper>
                    </div>
                </MuiThemeProvider>
            );
        }
    }
);

export default withStyles(styles)(ToDoList);
