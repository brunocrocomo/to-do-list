import { decorate, observable, action } from "mobx";

class ToDoStore {
    toDoList = [];

    checkListItem(id, checked) {
        this.toDoList[id].checked = checked;
    }

    addListItem(newItem) {
        this.toDoList = [...this.toDoList, newItem];
    }

    removeListItem(id) {
        this.toDoList = this.toDoList.filter((item, index) => index !== id);
    }
}

decorate(ToDoStore, {
    toDoList: observable,
    checkListItem: action,
    addListItem: action,
    removeListItem: action
});

export default ToDoStore;